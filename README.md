# README #

Matlab and python code to generate number of random walks, steps drawn from a Gaussian distribution.

### Includes exercises ###

* Drawing random numbers from Gaussian distribution.
* Matrix representation of each step in a walk for many different walks.
* Cumulative sum over a matrix in a given dimension.
* Averaging over a matrix in a given dimension.

### How do I get set up? ###

* Scripts are independent. Exercise files are provided for Neurophysics students.
* Clone or download the repository and run the files.

### Contribution guidelines ###

* Feedbacks, comments and push requests are welcomed.

### To-do ###

* 

### Contact ###

* Contact me from repo owner.