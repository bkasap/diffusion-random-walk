# Neurophysics 1 Diffusion Exercise
# Random walks and diffusion

import pylab as pl

nwalks = 100;                               # number of walks
nsteps = 500;                               # number of steps for each walk

for sigma in pl.linspace(2.5, 0.5, 5):      # simulating different sigma values
    Sx = sigma*pl.randn(nsteps, nwalks)     # normally distrubuted steps in x and y directions
    Sy = sigma*pl.randn(nsteps, nwalks)     #   for each step at different walks
    x = pl.cumsum(Sx, axis=0)               # cumulative sums: the total displacement in each direction
    y = pl.cumsum(Sy, axis=0)               #   after each step
    r = pl.square(x)+pl.square(y)           # r is the displacement for each step (squared)
    rmean = pl.mean(r, axis=1)              # average r for all walks

    # Plots
    pl.subplot(221)
    pl.plot(x[:,2], y[:,2])
    pl.title('One of the walks')
    pl.xlabel('x direction')
    pl.ylabel('y direction')
    
    pl.subplot(222)
    pl.plot(pl.sqrt(r[:,2]))
    pl.title('Position at each step')
    pl.xlabel('Nsteps')
    pl.ylabel('distance r')
    
    pl.subplot(223)
    pl.plot(x[-1,:], y[-1,:], '.')
    pl.title('Where each walk ends')
    pl.xlabel('x position')
    pl.ylabel('y position')
    
    pl.subplot(224)
    pl.plot(pl.linspace(0, nsteps, nsteps+1), 2*sigma*sigma*pl.linspace(0, nsteps, nsteps+1), 'k')
    pl.plot(rmean)
    pl.title('sigma= '+str(sigma))
    pl.xlabel('Nsteps')
    pl.ylabel('Average position <r2>')
    
    pl.pause(4)
