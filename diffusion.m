% Neurophysics 1 Diffusion Exercise
% Random walks and diffusion

clear all;
close all;
clc;

nwalks = 100;                               %number of walks
nsteps = 500;                               %number of steps for each walk

for sigma=2.5:-0.5:0.5                      % simulating different sigma values
    Sx = sigma*randn(nsteps,nwalks);        % normally distributed steps in x and y direction
    Sy = sigma*randn(nsteps,nwalks);        %   for each step at different walks
    x = cumsum(Sx);                         % cumulative sums: the total displacement in each direction
    y = cumsum(Sy);                         %   after each step
    r = x.^2+y.^2;                          % r is the displacement for each step (squared)
    rmean = mean(r, 2);                     % average r for all walks
    
    %Plots
    subplot(221)
    hold on;
    plot(x(:,2), y(:,2))
    title('One of the walks')
    xlabel('x direction')
    ylabel('y direction')
    
    subplot(222)
    hold on;
    plot(sqrt(r(:,2)));
    title('Position at each step');
    xlabel('Nsteps');
    ylabel('distance r')
    
    subplot(223)
    hold on;
    plot(x(end,:), y(end,:), '.');
    title('Where each walk ends')
    xlabel('x position')
    ylabel('y position')
    
    subplot(224)
    hold on;
    plot(linspace(0, nsteps, nsteps+1), 2*sigma*sigma*linspace(0, nsteps, nsteps+1), 'k');
    plot(rmean);
    title(strcat('sigma= ',num2str(sigma)));
    xlabel('Nsteps');
    ylabel('Average position <r2>');
    legend('theoretical', 'simulated', 'Location', 'northwest')
    legend('boxoff')
    
    pause(4);
end;
